//////////////////////////////////////
///  Dripper (working name) v0.1   ///
///  water drop controller, et al  ///
///  Copyright 2016 David Woods    ///
//////////////////////////////////////

//#define SERIAL_OUTPUT
#define FAKE_BUTTON
#define LED_SHUTTER

const int PIN_SHUTTER = 7;
const int PIN_VALVE = 6;
const int PIN_FLASH = 5;
#if defined(LED_SHUTTER)
  const int LED_ONBOARD = 13;
#endif

const int ON_SWITCH = 4;
int runState = 0;
int buttonState = 0;
int lastButtonState = 0;
#if defined(FAKE_BUTTON)
  bool hasFakedButton = false;
#endif

unsigned long startTime = 0;
unsigned long lastButtonDebounceTime = 0;
unsigned long buttonDebounceDelay = 50;

/**
 *  really good values!!
 *  const int MARK_drop1_open =      100;
 *  const int MARK_drop2_open =      245;
 *  const int MARK_flash =           665;
 *  const int DELAY_drop1_length =    15;
 *  const int DELAY_drop2_length =    20;
 */

// good values #2
//const int MARK_drop1_open =     100;
//const int MARK_drop2_open =     274;
//const int MARK_flash =          683;

const int MARK_drop1_open =     100;
const int MARK_drop2_open =     295;
const int MARK_flash =          710;

const int DELAY_drop1_length = 10;
const int DELAY_drop2_length = 10;
const int DELAY_flash_length = 100;
const int DELAY_flash_to_close_shutter = 50;

void setup() {
  #if defined(LED_SHUTTER)
    pinMode(LED_ONBOARD, OUTPUT);
  #endif
  
  pinMode(PIN_SHUTTER, OUTPUT);
  pinMode(PIN_VALVE, OUTPUT);
  pinMode(PIN_FLASH, OUTPUT);

  pinMode(ON_SWITCH, INPUT);
  
  #if defined(SERIAL_OUTPUT)
    Serial.begin(9600);
    Serial.println("SETUP");
  #endif
}

void openShutter() {
  if (digitalRead(PIN_SHUTTER) == HIGH) return;
  
  digitalWrite(PIN_SHUTTER, HIGH);
  #if defined(SERIAL_OUTPUT)
    Serial.println("open shutter");
  #endif
  #if defined(LED_SHUTTER)
    digitalWrite(LED_ONBOARD, HIGH);
  #endif
}

void closeShutter() {
  if (digitalRead(PIN_SHUTTER) == LOW) return;
  
  digitalWrite(PIN_SHUTTER, LOW);
  #if defined(SERIAL_OUTPUT)
    Serial.println("close shutter");
  #endif
  #if defined(LED_SHUTTER)
    digitalWrite(LED_ONBOARD, LOW);
  #endif
}

void valveOpen() {
  if (digitalRead(PIN_VALVE) == HIGH) return;
  
  digitalWrite(PIN_VALVE, HIGH);
  #if defined(SERIAL_OUTPUT)
    Serial.println("open valve");
  #endif
}

void valveClosed() {
  if (digitalRead(PIN_VALVE) == LOW) return;
  
  digitalWrite(PIN_VALVE, LOW);
  #if defined(SERIAL_OUTPUT)
    Serial.println("close valve");
  #endif
}

void flashOn() {
  if (digitalRead(PIN_FLASH) == HIGH) return;
  
  digitalWrite(PIN_FLASH, HIGH);
  #if defined(SERIAL_OUTPUT)
    Serial.println("flash on");
  #endif
}

void flashOff() {
  if (digitalRead(PIN_FLASH) == LOW) return;
  
  digitalWrite(PIN_FLASH, LOW);
  #if defined(SERIAL_OUTPUT)
    Serial.println("flash off");
  #endif
}

void runDrop() {
  if (runState == 0) return;
  
  unsigned long currentTime = millis();

  if (currentTime == startTime) {
    openShutter();
  }
  if (currentTime == startTime + MARK_drop1_open) {
    valveOpen();
  }
  if (currentTime == startTime + MARK_drop1_open + DELAY_drop1_length) {
    valveClosed();
  }
  if (currentTime == startTime + MARK_drop2_open) {
    valveOpen();
  }
  if (currentTime == startTime + MARK_drop2_open + DELAY_drop2_length) {
    valveClosed();
  }
  if (currentTime == startTime + MARK_flash) {
    flashOn();
  }
  if (currentTime == startTime + MARK_flash + DELAY_flash_length) {
    flashOff();
  }
  if (currentTime == startTime + MARK_flash + DELAY_flash_length + DELAY_flash_to_close_shutter) {
    closeShutter();
    runState = 0;
  }
}

void checkButton() {
  int reading = digitalRead(ON_SWITCH);
  #if defined(FAKE_BUTTON)
    if (hasFakedButton == false && runState == 0) {
      reading = HIGH;
    } else {
      reading = LOW;
    }
  #endif
  if (reading != lastButtonState) {
    lastButtonDebounceTime = millis();
  }
  if ((millis() - lastButtonDebounceTime) > buttonDebounceDelay) {
    // longer than the debounce delay, must be a clean button press
    if (reading != buttonState) {
      // oh hey, also the button is different
      buttonState = reading;
      if (buttonState == HIGH && runState == 0) {
        #if defined(FAKE_BUTTON)
          #if defined(SERIAL_OUTPUT)
            Serial.println("faking button");
          #endif
          if (hasFakedButton == false) {
            hasFakedButton = true;
            initRun();
          }
        #else
          // proceed with run now
          initRun();
        #endif
      }
    }
  }
  lastButtonState = reading;
}

void initRun() {
  if (runState == 0) {
    #if defined(SERIAL_OUTPUT)
      Serial.println("drop run cycle starting");
    #endif
    startTime = millis();
    runState = 1;
  }
}

void loop() {
  checkButton();
  runDrop();
}
